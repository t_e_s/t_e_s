#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias fdc='find . -name \*.c | xargs grep -n $1' 
alias fdh='find . -name \*.h | xargs grep -n  $1' 
alias fdj='find . -name \*.java | xargs grep -n $1' 
alias fdcpp='find . -name \*.cpp | xargs grep -n $1'
PS1='[\u@\h \W]\$ '
source ~/.git-completion.bash
