/*
 * =====================================================================================
 *
 *       Filename:  progress.c
 *
 *    Description:  i
 *
 *        Version:  1.0
 *        Created:  10/16/2013 04:10:40 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdlib.h>

#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/mman.h>
inline static unsigned short int make16color(unsigned char r, unsigned char g, unsigned char b)
{
    return ((((r >> 3) & 31) << 11) | (((g >> 2) & 63) << 5) | ((b >> 3) & 31)        );
}

int main() {
    int fbfd = 0;
    struct fb_var_screeninfo vinfo;
    struct fb_fix_screeninfo finfo;
    long int screensize = 0;
char *fbp = 0;
    int mul = 2 ;
    int x = 0, y = 0;
    int guage_height = 40, step = 200;
    long int location = 0;
    // Open the file for reading and writing
//    fbfd = open("/dev/graphics/fb0", O_RDWR);
    fbfd = open("/dev/fb0", O_RDWR);
    if (!fbfd) {
         printf("Error: cannot open framebuffer device.\n");
         exit(1);
    }
    printf("The framebuffer device was opened successfully.\n");
    // Get fixed screen information
    if (ioctl(fbfd, FBIOGET_FSCREENINFO, &finfo)) {
         printf("Error reading fixed information.\n");
         exit(2);
    }
    // Get variable screen information
    if (ioctl(fbfd, FBIOGET_VSCREENINFO, &vinfo)) {
         printf("Error reading variable information.\n");
         exit(3);
    }
printf("sizeof(unsigned short) = %d\n", sizeof(unsigned short));
    printf("%dx%d, %dbpp\n", vinfo.xres, vinfo.yres, vinfo.bits_per_pixel );
printf("xoffset:%d, yoffset:%d, line_length: %d\n", vinfo.xoffset, vinfo.yoffset, finfo.line_length );
    // Figure out the size of the screen in bytes
    screensize = vinfo.xres * vinfo.yres * vinfo.bits_per_pixel / 8;;
    // Map the device to memory
    fbp = (char *)mmap(0, screensize, PROT_READ | PROT_WRITE, MAP_SHARED,fbfd, 0);
    if ((int)fbp == -1) {
         printf("Error: failed to map framebuffer device to memory.\n");
         exit(4);
    }
    printf("The framebuffer device was mapped to memory successfully.\n");
    int bg = open("/mnt/blogo.bmp", O_RDWR);
    if (!bg) {
        close(fbfd);
         printf("Error: cannot open framebuffer device.\n");
         exit(1);
    }
//    memset(fbp, 0, screensize);
    sleep(1);
    int size = lseek(bg , 0 , SEEK_END);
    lseek(bg , 0 , SEEK_SET);
    printf("size is %d \n", read(bg,fbp,size));
    int div,x0,x1,x2,x3;
again:
    step = 50;
    div = (vinfo.xres / 8);
    x0 = div * 2 + div / 2 - step / 2;
    x1 = div * 3 + div / 2 - step / 2;
    x2 = div * 4 + div / 2 - step / 2;
    x3 = div * 5 + div / 2 - step / 2;

    y = (vinfo.yres - guage_height * 2) - 2;       // Where we are going to put the pixel
    for (x = x0; x < x0 + step; x++) {
        location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
        *((unsigned short int*)(fbp + location)) = 221;        // Some blue
        *((unsigned short int*)(fbp + location + 1)) = 255;
        *((unsigned short int*)(fbp + location + 2)) = 0;
        *((unsigned short int*)(fbp + location + 3)) = 0;
    }

    y = (vinfo.yres - guage_height) + 2;       // Where we are going to put the pixel
    for (x = x0; x < x0 + step; x++) {
        location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
        *((unsigned short int*)(fbp + location)) = 221;        // Some blue
        *((unsigned short int*)(fbp + location + 1)) = 255;
        *((unsigned short int*)(fbp + location + 2)) = 0;
        *((unsigned short int*)(fbp + location + 3)) = 0;
    }
    x = x0;
    for (y = (vinfo.yres - guage_height * 2) - 2; y < (vinfo.yres - guage_height) + 2; y++) {
         location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
        *((unsigned short int*)(fbp + location)) = 221;        // Some blue
        *((unsigned short int*)(fbp + location + 1)) = 255;
        *((unsigned short int*)(fbp + location + 2)) = 0;
        *((unsigned short int*)(fbp + location + 3)) = 0;
    }
    x = x0 + step;
    for (y = (vinfo.yres - guage_height * 2) - 2; y < (vinfo.yres - guage_height) + 2; y++) {
         location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
        *((unsigned short int*)(fbp + location)) = 221;        // Some blue
        *((unsigned short int*)(fbp + location + 1)) = 255;
        *((unsigned short int*)(fbp + location + 2)) = 0;
        *((unsigned short int*)(fbp + location + 3)) = 0;
    }
    // Figure out where in memory to put the pixel
    for ( x = x0 + 1; x < x0 + step; x++ ) {
         for ( y = (vinfo.yres - guage_height * 2) - 1; y < (vinfo.yres - guage_height) + 2; y++ ) {
             location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
             if ( vinfo.bits_per_pixel == 32 ) {
                *(fbp + location) = 221;        // Some blue
                *(fbp + location + 1) = 255;
                *(fbp + location + 2) = 0;
                *(fbp + location + 3) = 0;
             } else { //assume 16bpp
                unsigned char b = 255 * x / (vinfo.xres - step);
                unsigned char g = 255;     // (x - 100)/6 A little green
                unsigned char r = 255;    // A lot of red
                unsigned short int t = make16color(r, g, b);
                *((unsigned short int*)(fbp + location)) = t;
             }
        }
    }
    sleep(1);
    // Figure out where in memory to put the pixel
    for ( x = x0 + 1; x < x0 + step; x++ ) {
         for ( y = (vinfo.yres - guage_height * 2) -1; y < (vinfo.yres - guage_height) + 2; y++ ) {
             location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
             if ( vinfo.bits_per_pixel == 32 ) {
                *(fbp + location) = 0;        // Some blue
                *(fbp + location + 1) = 0;
                *(fbp + location + 2) = 0;
                *(fbp + location + 3) = 0;
             } else { //assume 16bpp
                unsigned char b = 255 * x / (vinfo.xres - step);
                unsigned char g = 255;     // (x - 100)/6 A little green
                unsigned char r = 255;    // A lot of red
                unsigned short int t = make16color(r, g, b);
                *((unsigned short int*)(fbp + location)) = t;
             }
        }
    }

    y = (vinfo.yres - guage_height * 2) - 2;       // Where we are going to put the pixel
    for (x = x1; x < x1 + step; x++) {
        location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
        *((unsigned short int*)(fbp + location)) = 221;        // Some blue
        *((unsigned short int*)(fbp + location + 1)) = 255;
        *((unsigned short int*)(fbp + location + 2)) = 0;
        *((unsigned short int*)(fbp + location + 3)) = 0;
    }

    y = (vinfo.yres - guage_height) + 2;       // Where we are going to put the pixel
    for (x = x1; x < x1 + step; x++) {
        location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
        *((unsigned short int*)(fbp + location)) = 221;        // Some blue
        *((unsigned short int*)(fbp + location + 1)) = 255;
        *((unsigned short int*)(fbp + location + 2)) = 0;
        *((unsigned short int*)(fbp + location + 3)) = 0;
    }
    x = x1;
    for (y = (vinfo.yres - guage_height * 2) - 2; y < (vinfo.yres - guage_height) + 2; y++) {
         location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
        *((unsigned short int*)(fbp + location)) = 221;        // Some blue
        *((unsigned short int*)(fbp + location + 1)) = 255;
        *((unsigned short int*)(fbp + location + 2)) = 0;
        *((unsigned short int*)(fbp + location + 3)) = 0;
    }
    x = x1 + step;
    for (y = (vinfo.yres - guage_height * 2) - 2; y < (vinfo.yres - guage_height) + 2; y++) {
         location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
        *((unsigned short int*)(fbp + location)) = 221;        // Some blue
        *((unsigned short int*)(fbp + location + 1)) = 255;
        *((unsigned short int*)(fbp + location + 2)) = 0;
        *((unsigned short int*)(fbp + location + 3)) = 0;
    }
    // Figure out where in memory to put the pixel
    for ( x = x1 + 1; x < x1 + step; x++ ) {
         for ( y = (vinfo.yres - guage_height * 2) - 1; y < (vinfo.yres - guage_height) + 2; y++ ) {
             location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
             if ( vinfo.bits_per_pixel == 32 ) {
                *(fbp + location) = 221;        // Some blue
                *(fbp + location + 1) = 255;
                *(fbp + location + 2) = 0;
                *(fbp + location + 3) = 0;
             } else { //assume 16bpp
                unsigned char b = 255 * x / (vinfo.xres - step);
                unsigned char g = 255;     // (x - 100)/6 A little green
                unsigned char r = 255;    // A lot of red
                unsigned short int t = make16color(r, g, b);
                *((unsigned short int*)(fbp + location)) = t;
             }
        }
    }
    sleep(1);
    // Figure out where in memory to put the pixel
    for ( x = x1 +1; x < x1 + step; x++ ) {
         for ( y = (vinfo.yres - guage_height * 2) - 1; y < (vinfo.yres - guage_height) + 2; y++ ) {
             location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
             if ( vinfo.bits_per_pixel == 32 ) {
                *(fbp + location) = 0;        // Some blue
                *(fbp + location + 1) = 0;
                *(fbp + location + 2) = 0;
                *(fbp + location + 3) = 0;
             } else { //assume 16bpp
                unsigned char b = 255 * x / (vinfo.xres - step);
                unsigned char g = 255;     // (x - 100)/6 A little green
                unsigned char r = 255;    // A lot of red
                unsigned short int t = make16color(r, g, b);
                *((unsigned short int*)(fbp + location)) = t;
             }
        }
    }

    y = (vinfo.yres - guage_height * 2) - 2;       // Where we are going to put the pixel
    for (x = x2; x < x2 + step; x++) {
        location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
        *((unsigned short int*)(fbp + location)) = 221;        // Some blue
        *((unsigned short int*)(fbp + location + 1)) = 255;
        *((unsigned short int*)(fbp + location + 2)) = 0;
        *((unsigned short int*)(fbp + location + 3)) = 0;
    }

    y = (vinfo.yres - guage_height) + 2;       // Where we are going to put the pixel
    for (x = x2; x < x2 + step; x++) {
        location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
        *((unsigned short int*)(fbp + location)) = 221;        // Some blue
        *((unsigned short int*)(fbp + location + 1)) = 255;
        *((unsigned short int*)(fbp + location + 2)) = 0;
        *((unsigned short int*)(fbp + location + 3)) = 0;
    }
    x = x2;
    for (y = (vinfo.yres - guage_height * 2) - 2; y < (vinfo.yres - guage_height) + 2; y++) {
         location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
        *((unsigned short int*)(fbp + location)) = 221;        // Some blue
        *((unsigned short int*)(fbp + location + 1)) = 255;
        *((unsigned short int*)(fbp + location + 2)) = 0;
        *((unsigned short int*)(fbp + location + 3)) = 0;
    }
    x = x2 + step;
    for (y = (vinfo.yres - guage_height * 2) - 2; y < (vinfo.yres - guage_height) + 2; y++) {
         location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
        *((unsigned short int*)(fbp + location)) = 221;        // Some blue
        *((unsigned short int*)(fbp + location + 1)) = 255;
        *((unsigned short int*)(fbp + location + 2)) = 0;
        *((unsigned short int*)(fbp + location + 3)) = 0;
    }
    // Figure out where in memory to put the pixel
    for ( x = x2 + 1; x < x2 + step; x++ ) {
         for ( y = (vinfo.yres - guage_height * 2) - 1; y < (vinfo.yres - guage_height) + 2; y++ ) {
             location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
             if ( vinfo.bits_per_pixel == 32 ) {
                *(fbp + location) = 221;        // Some blue
                *(fbp + location + 1) = 255;
                *(fbp + location + 2) = 0;
                *(fbp + location + 3) = 0;
             } else { //assume 16bpp
                unsigned char b = 255 * x / (vinfo.xres - step);
                unsigned char g = 255;     // (x - 100)/6 A little green
                unsigned char r = 255;    // A lot of red
                unsigned short int t = make16color(r, g, b);
                *((unsigned short int*)(fbp + location)) = t;
             }
        }
    }
    sleep(1);
    // Figure out where in memory to put the pixel
    for ( x = x2 + 1; x < x2 + step; x++ ) {
         for ( y = (vinfo.yres - guage_height * 2) - 1; y < (vinfo.yres - guage_height) + 2; y++ ) {
             location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
             if ( vinfo.bits_per_pixel == 32 ) {
                *(fbp + location) = 0;        // Some blue
                *(fbp + location + 1) = 0;
                *(fbp + location + 2) = 0;
                *(fbp + location + 3) = 0;
             } else { //assume 16bpp
                unsigned char b = 255 * x / (vinfo.xres - step);
                unsigned char g = 255;     // (x - 100)/6 A little green
                unsigned char r = 255;    // A lot of red
                unsigned short int t = make16color(r, g, b);
                *((unsigned short int*)(fbp + location)) = t;
             }
        }
    }

    y = (vinfo.yres - guage_height * 2) - 2;       // Where we are going to put the pixel
    for (x = x3; x < x3 + step; x++) {
        location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
        *((unsigned short int*)(fbp + location)) = 221;        // Some blue
        *((unsigned short int*)(fbp + location + 1)) = 255;
        *((unsigned short int*)(fbp + location + 2)) = 0;
        *((unsigned short int*)(fbp + location + 3)) = 0;
    }

    y = (vinfo.yres - guage_height) + 2;       // Where we are going to put the pixel
    for (x = x3; x < x3 + step; x++) {
        location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
        *((unsigned short int*)(fbp + location)) = 221;        // Some blue
        *((unsigned short int*)(fbp + location + 1)) = 255;
        *((unsigned short int*)(fbp + location + 2)) = 0;
        *((unsigned short int*)(fbp + location + 3)) = 0;
    }
    x = x3;
    for (y = (vinfo.yres - guage_height * 2) - 2; y < (vinfo.yres - guage_height) + 2; y++) {
         location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
        *((unsigned short int*)(fbp + location)) = 221;        // Some blue
        *((unsigned short int*)(fbp + location + 1)) = 255;
        *((unsigned short int*)(fbp + location + 2)) = 0;
        *((unsigned short int*)(fbp + location + 3)) = 0;
    }
    x = x3 + step;
    for (y = (vinfo.yres - guage_height * 2) - 2; y < (vinfo.yres - guage_height) + 2; y++) {
         location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
        *((unsigned short int*)(fbp + location)) = 221;        // Some blue
        *((unsigned short int*)(fbp + location + 1)) = 255;
        *((unsigned short int*)(fbp + location + 2)) = 0;
        *((unsigned short int*)(fbp + location + 3)) = 0;
    }
    // Figure out where in memory to put the pixel
    for ( x = x3 + 1; x < x3 + step; x++ ) {
         for ( y = (vinfo.yres - guage_height * 2) - 1; y < (vinfo.yres - guage_height) + 2; y++ ) {
             location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
             if ( vinfo.bits_per_pixel == 32 ) {
                *(fbp + location) = 221;        // Some blue
                *(fbp + location + 1) = 255;
                *(fbp + location + 2) = 0;
                *(fbp + location + 3) = 0;
             } else { //assume 16bpp
                unsigned char b = 255 * x / (vinfo.xres - step);
                unsigned char g = 255;     // (x - 100)/6 A little green
                unsigned char r = 255;    // A lot of red
                unsigned short int t = make16color(r, g, b);
                *((unsigned short int*)(fbp + location)) = t;
             }
        }
    }
    sleep(1);
    // Figure out where in memory to put the pixel
    for ( x = x3 + 1; x < x3 + step; x++ ) {
         for ( y = (vinfo.yres - guage_height * 2) - 1; y < (vinfo.yres - guage_height) + 2; y++ ) {
             location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
             if ( vinfo.bits_per_pixel == 32 ) {
                *(fbp + location) = 0;        // Some blue
                *(fbp + location + 1) = 0;
                *(fbp + location + 2) = 0;
                *(fbp + location + 3) = 0;
             } else { //assume 16bpp
                unsigned char b = 255 * x / (vinfo.xres - step);
                unsigned char g = 255;     // (x - 100)/6 A little green
                unsigned char r = 255;    // A lot of red
                unsigned short int t = make16color(r, g, b);
                *((unsigned short int*)(fbp + location)) = t;
             }
        }
    }
    goto again;
    munmap(fbp, screensize);
    close(fbfd);
    return 0;
//set to black color first
    //draw rectangle
    while(1){
    y = (vinfo.yres - guage_height * 2) - 2;       // Where we are going to put the pixel
    for (x = step - 2; x < vinfo.xres - step + 2; x++) {
        location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
         *((unsigned short int*)(fbp + location)) = 0;
    }
    y = (vinfo.yres - guage_height) + 2;       // Where we are going to put the pixel
    for (x = step - 2; x < vinfo.xres - step + 2; x++) {
         location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
         *((unsigned short int*)(fbp + location)) = 0;
    }
    x = step - 2;
    for (y = (vinfo.yres - guage_height * 2) - 2; y < (vinfo.yres - guage_height) + 2; y++) {
         location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
         *((unsigned short int*)(fbp + location)) = 0;
    }
    x = vinfo.xres - step + 2;
    for (y = (vinfo.yres - guage_height * 2) - 2; y < (vinfo.yres - guage_height) + 2; y++) {
         location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
         *((unsigned short int*)(fbp + location)) = 0;
    }
    // Figure out where in memory to put the pixel
    for ( x = step; x < vinfo.xres - step + 2; x++ ) {
         for ( y = (vinfo.yres - guage_height * 2); y < (vinfo.yres - guage_height) + 2; y++ ) {
             location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
             if ( vinfo.bits_per_pixel == 32 ) {
//                *(fbp + location) = 100;        // Some blue
//                *(fbp + location + 1) = 15+(x-100)/2;     // A little green
//                *(fbp + location + 2) = 200-(y-100)/5;    // A lot of red
//                *(fbp + location + 3) = 0;      // No transparency
                *(fbp + location) = 0;        // Some blue
                *(fbp + location + 1) = 0;
                *(fbp + location + 2) = 0;
                *(fbp + location + 3) = 0;
             } else { //assume 16bpp
                unsigned char b = 255 * x / (vinfo.xres - step);
                unsigned char g = 255;     // (x - 100)/6 A little green
                unsigned char r = 255;    // A lot of red
                unsigned short int t = make16color(r, g, b);
                *((unsigned short int*)(fbp + location)) = t;
             }
        }
   //printf("x = %d, temp = %d\n", x, temp);
         //sleep to see it
    }
//memset(fbp, 0, screensize);
    //draw rectangle
    y = (vinfo.yres - guage_height * 2) - 2;       // Where we are going to put the pixel
    for (x = step - 2; x < vinfo.xres - step + 2; x++) {
        location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
         *((unsigned short int*)(fbp + location)) = 255;
    }
    y = (vinfo.yres - guage_height) + 2;       // Where we are going to put the pixel
    for (x = step - 2; x < vinfo.xres - step + 2; x++) {
         location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
         *((unsigned short int*)(fbp + location)) = 255;
    }
    x = step - 2;
    for (y = (vinfo.yres - guage_height * 2) - 2; y < (vinfo.yres - guage_height) + 2; y++) {
         location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
         *((unsigned short int*)(fbp + location)) = 255;
    }
    x = vinfo.xres - step + 2;
    for (y = (vinfo.yres - guage_height * 2) - 2; y < (vinfo.yres - guage_height) + 2; y++) {
         location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
         *((unsigned short int*)(fbp + location)) = 255;
    }
    // Figure out where in memory to put the pixel
    for ( x = step; x < vinfo.xres - step + 2; x++ ) {
         for ( y = (vinfo.yres - guage_height * 2); y < (vinfo.yres - guage_height) + 2; y++ ) {
             location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;
             if ( vinfo.bits_per_pixel == 32 ) {
//                *(fbp + location) = 100;        // Some blue
//                *(fbp + location + 1) = 15+(x-100)/2;     // A little green
//                *(fbp + location + 2) = 200-(y-100)/5;    // A lot of red
//                *(fbp + location + 3) = 0;      // No transparency
                *(fbp + location) = 100;        // Some blue
                *(fbp + location + 1) = 100;
                *(fbp + location + 2) = 100;
                *(fbp + location + 3) = 100;
             } else { //assume 16bpp
                unsigned char b = 255 * x / (vinfo.xres - step);
                unsigned char g = 255;     // (x - 100)/6 A little green
                unsigned char r = 255;    // A lot of red
                unsigned short int t = make16color(r, g, b);
                *((unsigned short int*)(fbp + location)) = t;
             }
        }
   //printf("x = %d, temp = %d\n", x, temp);
         //sleep to see it
        usleep(10000);
    }
    }
    //clean framebuffer
    munmap(fbp, screensize);
    close(fbfd);
    return 0;
}

