#include <linux/i2c.h>
#include <linux/module.h>
#include <linux/types.h>

static struct i2c_client *i2c_wm8805 = NULL;

void wm8960_set(int rate)
{
	if(i2c_wm8805){
		printk("xxxxxxxxxx set rate is %d \n",rate);
		switch (rate) {
			case 44100:
				i2c_smbus_write_byte_data(i2c_wm8805, 0x00, 0x00);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x03, 0x89);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x04, 0xb0);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x05, 0x21);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x06, 0x07);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x07, 0x06);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x1e, 0x02);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x15, 0x60);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x16, 0xfb);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x1b, 0x0e);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x1c, 0xce);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x1d, 0x80);
				i2c_smbus_write_byte(i2c_wm8805,0);
				i2c_smbus_write_byte(i2c_wm8805,1);
				break;
			case 96000:
				i2c_smbus_write_byte_data(i2c_wm8805, 0x00, 0x00);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x03, 0xba);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x04, 0x49);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x05, 0x0c);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x06, 0x08);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x07, 0x15);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x1e, 0x02);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x15, 0x6a);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x16, 0x5b);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x1b, 0x0e);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x1c, 0xce);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x1d, 0x80);
				i2c_smbus_write_byte(i2c_wm8805,0);
				i2c_smbus_write_byte(i2c_wm8805,1);
				break;
			case 192000:
				i2c_smbus_write_byte_data(i2c_wm8805, 0x00, 0x00);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x03, 0xba);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x04, 0x49);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x05, 0x0c);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x06, 0x08);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x07, 0x2c);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x1e, 0x02);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x15, 0x6e);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x16, 0x1b);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x1b, 0x0e);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x1c, 0xce);
				i2c_smbus_write_byte_data(i2c_wm8805, 0x1d, 0x80);
				i2c_smbus_write_byte(i2c_wm8805,0);
				i2c_smbus_write_byte(i2c_wm8805,1);
				break;
		}
		printk("xxxxxxxxxx set rate is %d \n",rate);
	}
}
EXPORT_SYMBOL_GPL(wm8960_set);

static int __devinit wm8804_i2c_probe(struct i2c_client *client,
				      const struct i2c_device_id *id)
{
	int i = 0;
	i2c_wm8805 = client;
	for(i = 1;i<=0x2;i++)
		printk("@@@@@ wm8804 id reg %d is %02x \n",i ,i2c_smbus_read_byte_data(i2c_wm8805,i)); 
	return 0;
}

static int __devexit wm8804_i2c_remove(struct i2c_client *client)
{
	return 0;
}

static const struct i2c_device_id wm8804_id[] = {
	{ "wm8804", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, wm8804_id);

static struct i2c_driver wm8804_i2c_driver = {
	.driver = {
		.name	= "wm8804",
		.owner	= THIS_MODULE,
	},
	.probe		= wm8804_i2c_probe,
	.remove		= __devexit_p(wm8804_i2c_remove),
	.id_table	= wm8804_id,
};

static int __init wm8804_i2c_init(void)
{
	return i2c_add_driver(&wm8804_i2c_driver);
}
module_init(wm8804_i2c_init);

static void __exit wm8804_i2c_exit(void)
{
	i2c_del_driver(&wm8804_i2c_driver);
}
module_exit(wm8804_i2c_exit);

MODULE_AUTHOR("Jerry <jerry_xiao@ema-tech.com>");
MODULE_DESCRIPTION("Wm8804 codec I2C bus driver");
MODULE_LICENSE("GPL");
MODULE_ALIAS("i2c:wm8804");
