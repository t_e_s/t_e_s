#!/bin/sh -x

BOOTFS="/dev/mmcblk0p1"
DATAFS="/dev/mmcblk0p2"
ROOTFS="/dev/mmcblk0p3"
MLO="/tmp/MLO"
MLO_DEV="/dev/mtdblock0"
UBOOT="/tmp/u-boot.bin"
UBOOT_DEV="/dev/mtdblock1"
MACID="/tmp/macid"
MACID_DEV="/dev/mtdblock3"
RECOVERY="/tmp/uImage-initramfs"
RECOVERY_DEV="/dev/mtdblock5"

echo "+++ start +++"

tar xvf /tmp/mke2fs.tar.bz2 -C /
cat /etc/part_table_4g | fdisk /dev/mmcblk0
mkfs.vfat $BOOTFS -n boot
mkfs.vfat $DATAFS -n data
#mke2fs -T ext4 -O ^extent -L root -m0 $ROOTFS
dd if=$MLO of=$MLO_DEV
dd if=$UBOOT of=$UBOOT_DEV
dd if=$MACID of=$MACID_DEV
dd if=$RECOVERY of=$RECOVERY_DEV
